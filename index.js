'use strict';

const AdvancedError = require('./lib/advanced-error');

module.exports = AdvancedError;

'use strict';

const HTTPStatus = require('http-status');

/**
 * @extends Error
 */
class AdvancedError extends Error {
  /**
   * Creates a generic error.
   *
   * @param {object?} attr Attribute Object
   * @param {string?} attr.title Error title
   * @param {string?} attr.message Error message
   * @param {number?} attr.status HTTP status code of error.
   * @param {boolean?} attr.isPublic Whether the message should be visible to user or not.
   * @param {object?} attr.metadata Custom Metadata.
   */
  constructor(attr) {
    const attributes = attr || {};

    super(attributes.message || AdvancedError.DEFAULT_MESSAGE);

    this.title = attributes.title || AdvancedError.DEFAULT_TITLE;
    this.status = attributes.status || AdvancedError.DEFAULT_STATUS;
    this.metadata = attributes.metadata || {};

    if (typeof attributes.privacy !== 'boolean') {
      this.privacy = AdvancedError.DEFAULT_PRIVACY;
    } else {
      this.privacy = attributes.privacy;
    }
    this.isPublic = this.privacy; // Backward compatibility
    this.isOperational = true; // This is required since bluebird 4 doesn't append it anymore.

    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor.name);

    AdvancedError.runMiddlewares(this);
  }

  /**
   * Create a new instance of this using passed Error Exception
   *
   * @param {Error} error Native JS Error
   * @param {(string|Object)?} title - Error title Or Attributes Object.
   * @param {number?} status - HTTP status code of error.
   * @param {boolean?} privacy - Whether the message should be visible to user or not.
   * @param {object?} metadata Custom Metadata.
   * @returns {*} A new instance of this (can be AdvancedError or any extended class)
   */
  static createFromError(error, title, status, privacy, metadata) {
    if (!(error instanceof this)) {
      let params = {};

      switch (typeof title) {
        case 'string':
          params = {
            title,
            status,
            privacy,
            metadata
          };
          break;

        case 'object':
          params = title;
          break;

        default:
          break;
      }

      params.message = error.message;

      // Default privacy to false because a thrown Error should not be public by default
      if (typeof params.privacy !== 'boolean') {
        params.privacy = this.IS_PRIVATE;
      }

      const instance = new this(params);

      Error.captureStackTrace(error);

      return instance;
    } else {
      return error;
    }
  }

  /**
   * Add a global AdvancedError middleware
   *
   * @param {Function} func Function that can receive the error instance as first argument
   * @return {void} void
   */
  static use(func) {
    AdvancedError.MIDDLEWARES.push(func);
  }

  /**
   * Run each middlewares passing the current instance as first argument
   *
   * @param {AdvancedError} instance The current AdvancedError instance
   * @return {void} void
   */
  static runMiddlewares(instance) {
    for (const key in AdvancedError.MIDDLEWARES) {
      AdvancedError.MIDDLEWARES[key](instance);
    }
  }

  /**
   * Remove all middlewares
   *
   * @return {void} void
   */
  static resetMiddlewares() {
    AdvancedError.MIDDLEWARES = [];
  }
}

// Class constants / static attributes
AdvancedError.DEFAULT_TITLE = 'Error';
AdvancedError.DEFAULT_MESSAGE = 'Internal Server Error';
AdvancedError.DEFAULT_STATUS = HTTPStatus.INTERNAL_SERVER_ERROR;
AdvancedError.IS_PUBLIC = true;
AdvancedError.IS_PRIVATE = false;
AdvancedError.DEFAULT_PRIVACY = AdvancedError.IS_PUBLIC;
AdvancedError.MIDDLEWARES = [];

module.exports = AdvancedError;

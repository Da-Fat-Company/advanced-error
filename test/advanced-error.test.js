'use strict';

const expect = require('chai').expect;
const HTTPStatus = require('http-status');
const AdvancedError = require('../index');

describe('AdvancedError Tests', function() {
  it('Should throw an AdvancedError with default attributes', () => {
    const error = new AdvancedError();

    expect(() => { throw error; }).to.throw(AdvancedError);
    expect(error).to.have.deep.property('title'   , AdvancedError.DEFAULT_TITLE);
    expect(error).to.have.deep.property('message' , AdvancedError.DEFAULT_MESSAGE);
    expect(error).to.have.deep.property('status'  , AdvancedError.DEFAULT_STATUS);
    expect(error).to.have.deep.property('privacy', AdvancedError.DEFAULT_PRIVACY);
    expect(error).to.have.deep.property('isPublic', AdvancedError.DEFAULT_PRIVACY);
  });

  it('Should throw an AdvancedError with custom attributes passed as object', () => {
    const attributes = {
      title: 'foo',
      message: 'bar',
      status: HTTPStatus.BAD_REQUEST,
      privacy: AdvancedError.IS_PRIVATE,
      metadata: {
        foo: 'bar'
      }
    };
    const error = new AdvancedError(attributes);

    expect(() => { throw error; }).to.throw(AdvancedError);
    expect(error).to.have.deep.property('title'   , attributes.title);
    expect(error).to.have.deep.property('message' , attributes.message);
    expect(error).to.have.deep.property('status'  , attributes.status);
    expect(error).to.have.deep.property('privacy' , attributes.privacy);
    expect(error).to.have.deep.property('isPublic', attributes.privacy);
    expect(error).to.have.deep.property('metadata.foo', attributes.metadata.foo);
  });

  it('Should throw an AdvancedError from an Error using createFromError and default attributes', () => {
    const errorMsg = 'error';
    const error = new Error(errorMsg);
    const advancedError = AdvancedError.createFromError(error);

    expect(() => { throw advancedError; }).to.throw(AdvancedError);
    expect(advancedError).to.have.deep.property('title'   , AdvancedError.DEFAULT_TITLE);
    expect(advancedError).to.have.deep.property('message' , errorMsg);
    expect(advancedError).to.have.deep.property('status'  , AdvancedError.DEFAULT_STATUS);
    expect(advancedError).to.have.deep.property('privacy' , AdvancedError.IS_PRIVATE);
    expect(advancedError).to.have.deep.property('isPublic', AdvancedError.IS_PRIVATE);
  });

  it('Should throw an AdvancedError from an Error using createFromError and custom attributes passed as object', () => {
    const attributes = {
      title: 'foo',
      status: HTTPStatus.BAD_REQUEST,
      privacy: AdvancedError.IS_PRIVATE
    };
    const errorMsg = 'error';
    const error = new Error(errorMsg);
    const advancedError = AdvancedError.createFromError(error, attributes);

    expect(() => { throw advancedError; }).to.throw(AdvancedError);
    expect(advancedError).to.have.deep.property('title'   , attributes.title);
    expect(advancedError).to.have.deep.property('message' , errorMsg);
    expect(advancedError).to.have.deep.property('status'  , attributes.status);
    expect(advancedError).to.have.deep.property('privacy' , attributes.privacy);
    expect(advancedError).to.have.deep.property('isPublic', attributes.privacy);
  });

  it('Should throw an AdvancedError from an Error using createFromError ' +
     'and custom attributes passed as arguments (Legacy style)', () => {
    const attributes = {
      title: 'foo',
      status: HTTPStatus.BAD_REQUEST,
      privacy: AdvancedError.IS_PRIVATE
    };
    const errorMsg = 'error';
    const error = new Error(errorMsg);
    const advancedError = AdvancedError.createFromError(
      error,
      attributes.title,
      attributes.status,
      attributes.privacy
    );

    expect(() => { throw advancedError; }).to.throw(AdvancedError);
    expect(advancedError).to.have.deep.property('title'   , attributes.title);
    expect(advancedError).to.have.deep.property('message' , errorMsg);
    expect(advancedError).to.have.deep.property('status'  , attributes.status);
    expect(advancedError).to.have.deep.property('isPublic', attributes.privacy);
    expect(advancedError).to.have.deep.property('privacy', attributes.privacy);
  });

  it('Should throw an AdvancedError from an AdvancedError using createFromError', () => {
    const attributes = {
      title: 'foo',
      message: 'bar',
      status: HTTPStatus.BAD_REQUEST,
      privacy: AdvancedError.IS_PRIVATE,
      metadata: {
        foo: 'bar'
      }
    };
    const error = new AdvancedError(attributes);
    const advancedError = AdvancedError.createFromError(error);

    expect(() => { throw advancedError; }).to.throw(AdvancedError);
    expect(advancedError).to.have.deep.property('title'   , attributes.title);
    expect(advancedError).to.have.deep.property('message' , attributes.message);
    expect(advancedError).to.have.deep.property('status'  , attributes.status);
    expect(advancedError).to.have.deep.property('isPublic', attributes.privacy);
    expect(advancedError).to.have.deep.property('privacy' , attributes.privacy);
    expect(advancedError).to.have.deep.property('metadata.foo', attributes.metadata.foo);
  });

  let thisSouldBeTrue = false;

  it('Should throw an AdvancedError with a middleware', () => {
    const attributes = {
      title: 'foo',
      message: 'bar',
      status: HTTPStatus.BAD_REQUEST,
      privacy: AdvancedError.IS_PRIVATE,
      metadata: {
        foo: 'bar'
      }
    };

    AdvancedError.use(() => {
      thisSouldBeTrue = true;
    });

    const error = new AdvancedError(attributes);

    expect(() => { throw error; }).to.throw(AdvancedError);
    expect(thisSouldBeTrue).to.be.true;
  });

  it('Should throw an AdvancedError without a middleware by removing existing one', () => {
    const attributes = {
      title: 'foo',
      message: 'bar',
      status: HTTPStatus.BAD_REQUEST,
      privacy: AdvancedError.IS_PRIVATE,
      metadata: {
        foo: 'bar'
      }
    };

    thisSouldBeTrue = false;

    AdvancedError.resetMiddlewares();

    const error = new AdvancedError(attributes);

    expect(() => { throw error; }).to.throw(AdvancedError);
    expect(thisSouldBeTrue).to.be.false;
  });
});

